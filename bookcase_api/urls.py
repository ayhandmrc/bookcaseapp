from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index),
    url(r'^author/$', views.novelists_view),
    url(r'^author/(?P<novelist_id>\d+)/$', views.novelist_detail_view),
    url(r'^book/$', views.books_view_dimension),
    url(r'^book/(?P<book_id>\d+)/$', views.book_detail_view),
    url(r'^bookcase/$', views.bookcase_view),
]