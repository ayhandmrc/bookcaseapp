from datetime import datetime
from django.http import Http404, QueryDict
from bookcase_api.models import Novelist
from bookcase_http import BookcaseResponse


def get(r, novelist_id):
    try:
        return BookcaseResponse.custom_response(Novelist.to_dict(
            multi=False, pk=novelist_id
        ))
    except Novelist.DoesNotExist:
        return BookcaseResponse.record_not_found()


def put(r, novelist_id):
    return patch(r, novelist_id)


def patch(r, novelist_id):
    query = QueryDict(r.body).copy()
    params = ['name', 'surname', 'date_of_birth']
    params_map = [query.get(p, False) for p in params]
    requests_is_not_available = True in [
        False in params_map and r.method == 'PUT',
        all(i is False for i in params_map) and r.method == 'PATCH'
    ]
    if requests_is_not_available:
        return BookcaseResponse.invalid_request()
    elif not Novelist.fields_is_valid(query):
        return BookcaseResponse.invalid_request_parameters()
    else:
        try:
            novelist = Novelist.objects.get(pk=novelist_id)
            if novelist:
                unique_novelist = False
                if 'surname' in query:
                    unique_novelist= Novelist.objects.\
                        filter(surname__iexact=query['surname']).\
                        exclude(id=novelist_id)
                if 'date_of_birth' in query:
                    query['date_of_birth'] = datetime.strptime(
                        query['date_of_birth'],
                        '%d.%m.%Y'
                    )
                if unique_novelist:
                    return BookcaseResponse.response(
                        'error',
                        'This novelist already exists'
                    )
                else:
                    for field, value in query.items():
                        setattr(novelist, field, value)
                    novelist.save()
                    return BookcaseResponse.response(
                        'success',
                        'The novelist has been successfully updated'
                    )
        except Novelist.DoesNotExist:
            return BookcaseResponse.record_not_found()


def delete(r, novelist_id):
    try:
        novelist = Novelist.objects.get(pk=novelist_id)
        novelist.delete()
        return BookcaseResponse.response(
            'success',
            'The novelist has been successfully deleted'
        )
    except Novelist.DoesNotExist:
        return BookcaseResponse.record_not_found()


def other(r, novelist_id):
    raise Http404()